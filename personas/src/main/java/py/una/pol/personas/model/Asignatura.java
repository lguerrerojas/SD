package py.una.pol.personas.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@XmlRootElement
public class Asignatura  implements Serializable {

	Integer id_asignatura;
	String nombre;
	List<String> a;
	public Asignatura(){
		a = new ArrayList<String>();
	}

	public Asignatura(Integer id_asignatura, String nombre){
		this.id_asignatura = id_asignatura;
		this.nombre = nombre;
		a = new ArrayList<String>();
	}

	public Integer getId_asignatura() {
		return id_asignatura;
	}

	public void setId_asignatura(Integer id_asignatura) {
		this.id_asignatura = id_asignatura;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}